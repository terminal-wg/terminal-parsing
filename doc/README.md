# Documents / Papers

| Number            | Revision | Type   | Date       | Title                                 |
|:-----------------:|:--------:|:------:|:----------:|:--------------------------------------|
| [N0001](n0001.md) | R0       | Review | 2020-07-11 | Existing terminal sequence structures |
