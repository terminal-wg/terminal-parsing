# Roadmap

This document gives a basic outline of what we should focus on over time.

## Initial parser considerations

  - Unicode based, grammar derived from ECMA-48
  - manually created from transition table vs. built from parser generator
  - define some terminology
  - early ideas, that might lead to parser extensions:
    - `'DEC'`: vendor specifics from DEC (needed to reflect de facto standard in xterm likes)
    - `'WG'`: workgroup extension containing own definitions
    - `'ISO2022'`: ISO-2022 extension
    - `'UNICODE_CTRL'`: unicode control extension (maybe put under WG later on?)
    - `'UTF8'`: parser variant internally operating on UTF-8 bytes with limited UTF-8 parsing
    - more?
  - establish some sort of parsing levels based on used extensions

## Basic ECMA-48 grammar

  - find a suitable grammar notation, ideally paired with a parser generator to test it
  - identify sequence types defined in ECMA-48
  - fill in gaps like dealing with misplaced control codes (Can we get this regexp friendly?)
  - grammar format should be mostly self-documenting, highlight edge cases/pitfalls in additional documentation

## Basic ECMA-48 parser

  - basic parser can be a by-product of the grammar development, if we get a working parser from a generator
  - alternatively: pull states/transitions from "grounded" generated parser and build the stream version manually
  - extensive tests with crafted and real world testdata
  - final goal: reference parser
  - maybe or later on: reference parser realized in C, Python, Java
  - documentation

## DEC extension

  - basic DEC compatibility is a must-have to reflect current de facto standard among xterm-likes
  - extend grammar with DEC specific rules, like DCS subformat and error recovery (taken from VT100.net's parser)
  - write some sort of base parser extension/plugin (needs some thinking about extensibility of the base parser, alternatively simply do a full parser bootstrap with altered grammars)
  - document differences to ECMA-48 parser

## Optional extensions

The following ideas are nice to have, but they are not mandatory to get basic terminal parsing done.

### WG extension

  - we most likely want this extension for our own sake
  - identify grammar rules we want to place under the workgroup extension, candidates:
    - OSC subformat with numerical identifier
    - BEL as finalizer in OSC
    - others?
  - must be compatible with base parser and DEC extension
  - write some sort of base parser extension/plugin
  - documentation about differences

### UTF8 extension

  - discuss usefulness of partially pulling UTF-8 parsing into the parser
  - write plugin with extended parser states and interfaces with byte like types
  - UTF-8 error handling?

### ISO2022 extension

  - investigate usefulness (almost dead?)
  - implement tiny terminal structure to hold code tables
  - proper access interface to load charsets and set conformance levels
  - provide some standard charsets and registering API

### UNICODE_CTRL extension

  - investigate usefulness to integrate certain unicode controls into the parser
  - hottest candidates: controls that alter text representation in a "terminal aware" way (bidi, joiner, graphemes)

## Code examples

This may include anything around dealing with terminal data in different languages:

  - simple filter tasks
  - full sequence evaluation
  - regexp examples pulled from the grammar
  - optimization ideas
  - ideas for restricted envs
  - parser interface ideas (push/callback based vs. pull)

Examples may be created while other positions are still under construction. They should be in a working condition and testable to some degree, to not run out of sync with changes on other ends.
